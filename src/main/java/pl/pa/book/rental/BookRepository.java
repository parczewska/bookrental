package pl.pa.book.rental;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BookRepository {

    private List<Book> books = new ArrayList<>();

    public Optional<Book> find(Book book) {
        return books.stream().filter(e -> e.equals(book)).findFirst();
    }

    public void add(Book book) {
        books.add(book);
    }

    public void delete(Book book) {
        books.remove(book);
    }

    @Override
    public String toString() {
        return "BookRepository{" +
                "books=" + books +
                '}';
    }
}
