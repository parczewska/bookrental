package pl.pa.book.rental;

import java.util.Optional;

public class Reantal {

    private BookRepository bookRepository;

    public Reantal(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public void borrow(Book book, Borrower borrower) {
        Optional<Book> resul = bookRepository.find(book);

        if (resul.isPresent()) {
            if (borrower.borrow(book)) {
                bookRepository.delete(book);
            }
        } else {
            System.out.println("Nie ma takiej książki!" + book);
        }
    }

    public void giveBack(Book book, Borrower borrower) {
        if (borrower.giveBack(book)) {
            bookRepository.add(book);
        } else {
            System.out.println("Wypożyczający nie posiada takiej książki!");
        }
    }
}
