package pl.pa.book.rental;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Borrower {

    private final long id;
    private final String name;
    private final String lastName;
    private List<Book> books = new ArrayList<>();

    public Borrower(String name, String lastName) {
        this.id = new Random().nextLong();
        this.name = name;
        this.lastName = lastName;
    }

    public boolean borrow(Book book) {
        if (books.size() < 5) {
            books.add(book);
            return true;
        }
        System.out.println("Wypożyczyłeś już 5 książek!");
        return false;
    }

    public boolean giveBack(Book book) {
        return books.remove(book);
    }

    @Override
    public String toString() {
        return "Borrower{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", books=" + books +
                '}';
    }
}
